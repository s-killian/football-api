import $ from 'jquery';

export default class Background {
	constructor () {
		this.initEls();
		this.initEvents();
	}

	initEls () {
		this.$els = {
			background: $('.js-background'),
		};
		this.url = 'https://source.unsplash.com/collection';
		this.cat = '2540303';
		this.size = '1920x1080';
	}

	initEvents () {
		this.loadImage();
	}

	loadImage () {
		const promise = new Promise( (resolve, reject) => {
			const image = new Image();
			image.src = `${this.url}/${this.cat}/${this.size}`;
			image.onload = () => {
				resolve(image);
			};

			image.onerror = (error) => {
				reject(error);
			};

			});

			promise.then( (image) => {
			this.addBackground(image);

			});

			promise.catch( (error) => {
			console.log(error);

			});

	}

	addBackground (image) {

		console.log(image);
		this.$els.background.css('background-image', `url(${image.src})`);

	}

}



