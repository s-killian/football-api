export function getHour () {
	let timeOfDay;
	
	let date = new Date();
	let hour = date.getHours();

	if ( hour < 12 ) {
		timeOfDay = 'morning';
	} else if (hour > 12 && hour < 17) {
		timeOfDay = 'afternoon';
	} else {
		timeOfDay = 'evening';
	}

	return timeOfDay
}