	
import $ from 'jquery';

export default class Loadteam {
	constructor () {
		this.initEls();
		this.initEvents();
	}

	initEls () {

	}

	initEvents () {
        this.loadTeam();
        this.displayTeamContent();
    }
    
    displayTeamContent() {

		let l2england;
		let l2french;

		const api = {
			endpoint: 'https://allsportsapi.com/api/football/',
			params: {
				'met':'Standings',
				'leagueId':'177',
				'APIkey': '713757ea6e68bc2086e47fc002ead16b54a3870da52a9dcc3715fa76d2dd4ac1'
			}

		};

		const api2 = {
			endpoint: 'https://allsportsapi.com/api/football/',
			params: {
				'met':'Standings',
				'leagueId':'423',
				'APIkey': '713757ea6e68bc2086e47fc002ead16b54a3870da52a9dcc3715fa76d2dd4ac1'
			}

		};

		$.getJSON(api.endpoint, api.params)
			.then((response) => {
				
				l2french = response.result.total;
        		
			})
			.catch((e) => {
				console.log('error', e)
			});

		$.getJSON(api2.endpoint, api2.params)
			.then((response) => {
				
				l2england = response.result.total;
        		
			})
			.catch((e) => {
				console.log('error', e)
			});


		$('.team__content__english').click(function() {
			console.log(l2england);
			$('.team__content').hide();
			l2england.forEach(function(element) {
				let team_place = element.standing_place;
				let team_name = element.standing_team;
				let team_points = element.standing_PTS;
				let team_win = element.standing_W;
				let team_def = element.standing_D;
				$('.team__standings').append(team_place + ' ', team_name + ' ', team_points + ' ', team_win + ' ', team_def + '<br>');

			});

		});
		
        $('.team__content__french').click(function() {
			$('.team__content').hide();
			l2french.forEach(function(element) {
				let team_place = element.standing_place;
				let team_name = element.standing_team;
				let team_points = element.standing_PTS;
				let team_win = element.standing_W;
				let team_def = element.standing_D;
				$('.team__standings').append(team_place + ' ', team_name + ' ', team_points + ' ', team_win + ' ', team_def + '<br>');

			});

		});
    }


	loadTeam() {

		const api = {
			endpoint: 'https://allsportsapi.com/api/football/',
			params: {
				'met':'Leagues',
				'APIkey': '713757ea6e68bc2086e47fc002ead16b54a3870da52a9dcc3715fa76d2dd4ac1'
			}

		};

		$.getJSON(api.endpoint, api.params)
			.then((response) => {
				console.log(response);
				$('.team__content__french__teamname').html(response.result[0].league_name);
				$('.team__content__english__teamname').html(response.result[1].league_name);
        		
			})
			.catch((e) => {
				console.log('error', e)
			});

	}

}
