	
import $ from 'jquery';

export default class Team {
	constructor () {
		this.initEls();
		this.initEvents();
	}

	initEls () {
		this.$els = {
			quote: $('.js-team'),
		};
	}

	initEvents () {
		this.getTeam();
	}


	getTeam() {

		const api = {
			endpoint: 'https://allsportsapi.com/api/football/',
			params: {
				'met':'Leagues',
				'APIkey': '713757ea6e68bc2086e47fc002ead16b54a3870da52a9dcc3715fa76d2dd4ac1'
			}

		};

		$.getJSON(api.endpoint, api.params)
			.then((response) => {
				console.log(response);
				$('.team__content__french__teamname').html(response.result[0].league_name);
				$('.team__content__english__teamname').html(response.result[1].league_name);
        		
			})
			.catch((e) => {
				console.log('error', e)
			});

	}

}
