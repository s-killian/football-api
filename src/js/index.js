import '../scss/style.scss';
import Background from './background';
import Team from './team';
import Loadteam from './loadteam'

class App {
    constructor () {
	  this.initApp();
	}

	initApp () {
	  this.background = new Background();
		this.quote = new Team();
		this.loadteam = new Loadteam();
	}

}
new App();
