const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  // Mode can be 'development' or 'production'
  mode: 'production',
  entry: './src/js/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  },

  module: {
    rules: [

    // Cherche les fichiers html et les ressort dans le bon dossier //

      { 
      test: /\.html$/,
      use: 'html-loader'
      },

      { 
      test: /\.js$/,
      use: 'babel-loader'
      },


      // Cherche les SCSS, charge le SASS, puis le POSTCSS,   //
      // puis le CSS-LOADER et enfin le MiniCssExtractPlugin  //

      { 

         test: /\.scss$/,
         use: [
          { 
              loader: MiniCssExtractPlugin.loader,
              options: {
              publicPath: '../'
            }
          },
          'css-loader', 

          {
            loader: 'postcss-loader',
            options: {
              config: {
                ctx: {
                  env: 'production'
                }
              }
            }
          }, 'sass-loader'
          
          ]

      },

      // Cherche les fichiers images et les ressort dans me même path,   //


      {
        test: /\.(png|jpg|gif)$/,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]',
          context: path.resolve(__dirname, 'src'),
        },
      }

      ]

      //fin du rules

  },

      // Chargement des plugins

  plugins: [

      new HtmlWebpackPlugin({
        template:'src/index.html',
        filename: 'index.html'
      }),

      new MiniCssExtractPlugin({
        filename: 'css/style.css'
      }),


    ]

};


